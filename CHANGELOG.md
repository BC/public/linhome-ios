# Change Log
All notable changes to this project will be documented in this file.

Group changes to describe their impact on the project, as follows:

    Added for new features.
    Changed for changes in existing functionality.
    Deprecated for once-stable features removed in upcoming releases.
    Removed for deprecated features removed in this release.
    Fixed for any bug fixes.
    Security to invite users to upgrade in case of vulnerabilities.

## [1.3.0]
- Migration to Flexiapi for account management
- Update to Linphone SDK 5.3 

## [1.2.0]
- Locally defined Devices are now stored inside VCards 4.0 - inside Linphone Friend List (those can be edited / removed)
- Devices can remotely provisionned, (they appear as read only - e.g. not deletable, not editable) inside the app.
- Update to Linphone SDK 5.2

## [1.1.0] 
- Update to Linphone SDK 5.0.0
- German translation
- Fix delete button not showing on recent iOS in history view
- Fix history events marked as read only if played
- Fix Decline incoming call if already on a call
- Fix ring with app opened and interaction with vibrator
- Set keyboard type to phonePad for action digits so it supports # and *

## [1.0.0] Release candidate
